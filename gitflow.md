# gitflow

## Manual branching

This is how the **gitflow** is supposed to be used.

**Always** this is **the first step**: you check to branch `devel` because that is where we always start.

If you have something that isn't yet `commit`-ed you won't be able to switch so you have to commit your changes prior to this step.

    $ git checkout devel

Once you are on `devel`, you have to `pull` latest changes for the server (`repository` or **repo**) 

    $ git pull

With this new state of code, with all the previous work merged into `devel` branch, you are ready to start working. **All new work** must be on new, separate branch.

    $ git checkout -b feature/3-my-new-task

These are the steps you **repeat** for each meaningful amount of code. If in doubt, those are actually subtasks, parts that are confirmed working and points you may wish to revert to.

    $ git add .
    $ git commit -m "Description that really says something meaningful."

You can try and test, even delete commits while they are still on your machine, **everything is still possible.**

Then, only once you are sure that code should be shared (e.g. sent to server) you should make a `push`.

**Prior** to push you should check if code work as intended and perform testing to your best knowledge to be sure task is done well, code documented and tested that it solves issue for which the branch is opened.

    $ git push

After pushing you are usually notified of successful push and offered a link to merge it into default branch (`devel`)

    $ git push
    Counting objects: 3, done.
    Delta compression using up to 8 threads.
    Compressing objects: 100% (3/3), done.
    Writing objects: 100% (3/3), 501 bytes | 0 bytes/s, done.
    Total 3 (delta 0), reused 0 (delta 0)
    remote: 
    remote: To create a merge request for 2-add-mobile-meta, visit:
    remote:   https://gitlab.com/MacMladen/mladen-casablanca/merge_requests/new?merge_request%5Bsource_branch%5D=2-add-mobile-meta
    remote: 
    To gitlab.com:MacMladen/mladen-casablanca.git
       eea63f7..fa1ed69  2-add-mobile-meta -> 2-add-mobile-meta

Use that link to finish request in browser.

Once you are notified of successful `merge`(by mail or **slack** if integration is made) you should repeat from first step.

    Merge Request !2 was merged

## Gitlab flow

It is relatively similar to this because it is the same branching, except we use **Gitlab** project management to create branch for issue.

**Every task starts** with opening a **new issue** on Gitlab.

After opening an issue, create a new branch in Gitlab for that issue.

To get that new branch you have to first get that branch from server (of course, everything starts from devel but it is not important for this workflow. However, you should keep `devel` current)

    $ git checkout devel
    $ git pull

Get new branch and switch to it

    $ git fetch
    $ git checkout 3-new-branch

Just like previously, these are the steps you **repeat** for each meaningful amount of code. If in doubt, those are actually subtasks, parts that are confirmed working and points you may wish to revert to.

    $ git add .
    $ git commit -m "Description that really says something meaningful."

Everything after this is the same essentially

    $ git push

After pushing you are usually notified of successful push and offered a link to merge it into default branch (`devel`)

After merging, you continue to next task (`issue`).
