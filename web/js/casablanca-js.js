/*
*Casablanca JS file
 */

// isotope

// var $grid = $('.grid').isotope({
//     // options
//     itemSelector: '.grid-item',
//     layoutMode: 'fitRows'
// });
//
// $('.filter-button-group').on( 'click', 'button', function() {
//     var filterValue = $(this).attr('data-filter');
//     $grid.isotope({ filter: filterValue });
// });

// init Isotope
var $grid = $('.isotope').isotope({
    // options
    "itemSelector": ".isotope-item",
    "layoutMode": "fitRows"
});
// filter items on button click
$('.isotope-buttons').on( 'click', 'button', function() {
    var filterValue = $(this).attr('data-filter');
    $grid.isotope({ filter: filterValue });
});
